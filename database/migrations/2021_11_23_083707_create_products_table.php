<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('brand_id')->nullable();
            $table->string('url')->nullable();
            $table->string('name')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->text('annotation')->nullable();
            $table->text('description')->nullable();
            $table->text('body')->nullable();
            $table->string('video')->nullable();
            $table->string('attachment_file')->nullable();
            $table->text('delivery')->nullable();
            $table->text('provider')->nullable();
            $table->integer('featured')->default('0');
            $table->integer('likes')->default('0');
            $table->integer('views')->default('0');
            $table->integer('votes')->default('0');
            $table->float('rating', 3, 1)->default('0.0');
            $table->uuid('external_id')->nullable();
            $table->integer('position')->default('0');
            $table->boolean('enabled')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
