<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('np_cities', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('description')->index()->nullable();
            $table->uuid('ref')->nullable();
            $table->uuid('area')->nullable();
            $table->string('area_description')->nullable();
            $table->string('area_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('np_cities');
    }
}
