<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('np_warehouses', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('description')->nullable();
            $table->string('short_address')->nullable();
            $table->string('phone')->nullable();
            $table->uuid('ref')->nullable();
            $table->uuid('city_ref')->index()->nullable();
            $table->string('city_description')->nullable();
            $table->string('area')->nullable();
            $table->string('area_description')->nullable();
            $table->string('area_type')->nullable();
            $table->string('region_description')->nullable();
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('np_warehouses');
    }
}
