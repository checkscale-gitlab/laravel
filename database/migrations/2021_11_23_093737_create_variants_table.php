<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variants', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('product_id')->nullable();
            $table->uuid('external_id')->nullable();
            $table->uuid('currency_id')->nullable();
            $table->string('sku')->nullable();
            $table->decimal('price', 10, 2)->default('0.00');
            $table->decimal('compare_price', 10, 2)->default('0.00');
            $table->string('attachment')->nullable();
            $table->decimal('weight', 5, 1)->default('0.4');
            $table->integer('amount')->default('0');
            $table->integer('position')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variants');
    }
}
