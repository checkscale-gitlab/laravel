<?php

namespace Database\Seeders;

use App\Models\Group;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        $users    = [];
        $password = Hash::make('!123456Qwerty');
        $faker    = Faker::create('ru_RU');
        $group    = Group::all()->pluck('id')->toArray();

        for ($i = 1; $i <= 10; $i++) {
            $users[] = [
                'id'                => $faker->uuid,
                'group_id'          => Arr::random($group),
                'name'              => $faker->name,
                'email'             => $faker->email,
                'email_verified_at' => now()->addMinutes($i),
                'phone'             => $faker->phoneNumber,
                'photo'             => $faker->image,
                'address'           => $faker->address,
                'city'              => $faker->city,
                'last_ip'           => $faker->ipv4,
                'password'          => $password,
                'baned'             => false,
                'remember_token'    => null,
                'updated_at'        => now()->addMinutes($i),
                'created_at'        => now()->addMinutes($i),
            ];
        }

        DB::table('users')->insert($users);

    }
}

