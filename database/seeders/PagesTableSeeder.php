<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace Database\Seeders;

use App\Models\Menu;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PagesTableSeeder extends Seeder
{
    public function run()
    {
        $pages = [];
        $faker = Faker::create('en_EN');
        $menus = Menu::all()->pluck('id')->toArray();

        for ($i = 1; $i <= 50; $i++) {
            $name  = $faker->company;
            $pages[] = [
                'id'               => $faker->uuid,
                'url'              => Str::of($name)->slug('_'),
                'name'             => $name,
                'meta_title'       => $name . ' ' . $faker->text($maxNbChars = 90),
                'meta_description' => $name . ' ' . $faker->text($maxNbChars = 150),
                'description'      => $name . ' ' . $faker->text($maxNbChars = 500),
                'menu_id'          => Arr::random($menus),
                'icon'             => $faker->imageUrl($width = 250, $height = 250),
                'visible'          => Arr::random([true, false]),
                'updated_at'       => now()->addMinutes($i),
                'created_at'       => now()->addMinutes($i),
            ];
        }

        DB::table('pages')->insert($pages);
    }
}

