<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class DeliveriesTableSeeder extends Seeder
{
    public function run()
    {
        $delivery = [];
        $faker    = Faker::create('ru_RU');

        for ($i = 1; $i <= 15; $i++) {

            $delivery[] = [
                'id'               => $faker->uuid,
                'name'             => $faker->company,
                'description'      => $faker->text($maxNbChars = 2000),
                'free_from'        => random_int(50, 500) . '.' . random_int(10, 99),
                'price'            => random_int(50, 150) . '.' . random_int(10, 99),
                'enabled'          => true,
                'position'         => $i,
                'separate_payment' => Arr::random([true, false]),
                'image'            => $faker->imageUrl($width = 250, $height = 250),
                'updated_at'       => now()->addMinutes($i),
                'created_at'       => now()->addMinutes($i),
            ];
        }

        DB::table('deliveries')->insert($delivery);
    }
}

