<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace Database\Seeders;

use App\Models\Categories;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CategoriesTableSeeder extends Seeder
{
    public function run()
    {
        // add new parent categories
        $categories = [];
        $faker      = Faker::create('ru_RU');

        for ($i = 1; $i <= 10; $i++) {

            $category_name = $faker->sentence($nbWords = 2, $variableNbWords = true);

            $categories[] = [
                'id'               => $faker->uuid,
                'parent_id'        => null,
                'name'             => $category_name,
                'meta_title'       => $faker->text($maxNbChars = 50),
                'meta_description' => $faker->text($maxNbChars = 90),
                'title'            => $faker->text($maxNbChars = 50),
                'description'      => $faker->text($maxNbChars = 2000),
                'annotation'       => $faker->text($maxNbChars = 500),
                'url'              => Str::of($category_name)->slug('_'),
                'image'            => $faker->imageUrl($width = 250, $height = 250),
                'visible'          => true,
                'position'         => $i,
                'updated_at'       => now()->addMinutes($i),
                'created_at'       => now()->addMinutes($i),
            ];
        }

        DB::table('categories')->insert($categories);

        // Add new sub-categories from category relation on parent_id on exist categories
        $categories     = null;
        $sub_categories = [];
        $faker          = Faker::create('ru_RU');
        $categories     = Categories::all()->pluck('id')->toArray();

        for ($i = 1; $i <= 100; $i++) {

            $subcategory_name = $faker->sentence($nbWords = 2, $variableNbWords = true);

            $sub_categories[] = [
                'id'               => $faker->uuid,
                'parent_id'        => Arr::random($categories),
                'name'             => $subcategory_name,
                'meta_title'       => $faker->text($maxNbChars = 50),
                'meta_description' => $faker->text($maxNbChars = 90),
                'title'            => $faker->text($maxNbChars = 50),
                'description'      => $faker->text($maxNbChars = 2000),
                'annotation'       => $faker->text($maxNbChars = 500),
                'url'              => Str::of($subcategory_name)->slug('_'),
                'image'            => $faker->imageUrl($width = 250, $height = 250),
                'visible'          => true,
                'position'         => $i,
                'updated_at'       => now()->addMinutes($i),
                'created_at'       => now()->addMinutes($i),
            ];
        }

        DB::table('categories')->insert($sub_categories);

    }
}

