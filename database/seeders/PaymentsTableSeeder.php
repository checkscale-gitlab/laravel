<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace Database\Seeders;

use App\Models\Currencies;
use Exception;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Faker\Provider\uk_UA\Payment as Payment;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class PaymentsTableSeeder extends Seeder
{
    /**
     * @throws Exception
     */
    public function run()
    {
        $payments   = [];
        $faker      = Faker::create('en_EN');
        $currencies = Currencies::all()->pluck('id')->toArray();

        for ($i = 1; $i <= 20; $i++) {
            $bank = Payment::bank();
            $payments[] = [
                'id'          => $faker->uuid,
                'service'     => Str::of($bank)->slug('_'),
                'name'        => $bank,
                'description' => $bank . ' ' . $faker->text($maxNbChars = 500),
                'currency_id' => Arr::random($currencies),
                'settings'    => $faker->text($maxNbChars = 250),
                'image'       => $faker->imageUrl($width = 250, $height = 250),
                'position'    => $i,
                'enabled'     => true,
                'updated_at'  => now()->addMinutes($i),
                'created_at'  => now()->addMinutes($i),
            ];
        }

        DB::table('payments')->insert($payments);
    }
}

