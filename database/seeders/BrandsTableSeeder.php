<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class BrandsTableSeeder extends Seeder
{
    public function run()
    {
        $brands = [];
        $faker = Faker::create('en_EN');

        for ($i = 1; $i <= 50; $i++) {
            $brand_name = $faker->company;
            $brands[]     = [
                'id'               => $faker->uuid,
                'name'             => $brand_name,
                'url'              => Str::of($brand_name)->slug('_'),
                'meta_title'       => $brand_name . ' ' . $faker->text($maxNbChars = 50),
                'meta_description' => $brand_name . ' ' . $faker->text($maxNbChars = 90),
                'description'      => $brand_name . ' ' . $faker->text($maxNbChars = 2000),
                'annotation'       => $brand_name . ' ' . $faker->text($maxNbChars = 500),
                'image'            => $faker->imageUrl($width = 250, $height = 250),
                'visible'          => true,
                'position'         => $i,
                'updated_at'       => now()->addMinutes($i),
                'created_at'       => now()->addMinutes($i),
            ];
        }

        DB::table('brands')->insert($brands);

    }
}

