<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PostsTableSeeder extends Seeder
{
    public function run()
    {
        $blogs = [];
        $faker = Faker::create('ru_RU');

        for ($i = 1; $i <= 50; $i++) {

            $name = $faker->sentence($nbWords = 2, $variableNbWords = true);

            $blogs[] = [
                'id'               => $faker->uuid,
                'name'             => $name,
                'url'              => Str::of($name)->slug('_'),
                'meta_title'       => $faker->text($maxNbChars = 50),
                'meta_description' => $faker->text($maxNbChars = 90),
                'title'            => $faker->text($maxNbChars = 50),
                'description'      => $faker->text($maxNbChars = 2000),
                'annotation'       => $faker->text($maxNbChars = 500),
                'image'            => $faker->imageUrl($width = 800, $height = 600),
                'type'             => Arr::random(['blog', 'news']),
                'visible'          => true,
                'updated_at'       => now()->addMinutes($i),
                'created_at'       => now()->addMinutes($i),
            ];
        }

        DB::table('posts')->insert($blogs);

    }
}

