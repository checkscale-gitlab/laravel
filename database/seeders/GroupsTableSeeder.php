<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class GroupsTableSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker::create('ru_RU');

        $groups = [
            [
                'id'         => $faker->uuid,
                'name'       => 'Новичок',
                'discount'   => '0.00',
                'updated_at' => now(),
                'created_at' => now(),
            ],
            [
                'id'         => $faker->uuid,
                'name'       => 'Опытный',
                'discount'   => '2.00',
                'updated_at' => now(),
                'created_at' => now(),
            ],
            [
                'id'         => $faker->uuid,
                'name'       => 'Постоянный',
                'discount'   => '5.00',
                'updated_at' => now(),
                'created_at' => now(),
            ],
            [
                'id'         => $faker->uuid,
                'name'       => 'Оптовый',
                'discount'   => '8.00',
                'updated_at' => now(),
                'created_at' => now(),
            ],
        ];

        DB::table('groups')->insert($groups);

    }
}

