<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class LabelsTableSeeder extends Seeder
{
    /**
     * @throws Exception
     */
    public function run()
    {
        $labels = [
            [
                'id'         => Str::uuid(),
                'name'       => 'Перезвонить',
                'color'       => '01CA0D',
                'position'   => 1,
                'updated_at' => now(),
                'created_at' => now(),
            ],
            [
                'id'         => Str::uuid(),
                'name'       => 'Срочно',
                'color'       => 'F2AD02',
                'position'   => 2,
                'updated_at' => now(),
                'created_at' => now(),
            ],
            [
                'id'         => Str::uuid(),
                'name'       => 'Не звонить',
                'color'       => '00A0E5',
                'position'   => 3,
                'updated_at' => now(),
                'created_at' => now(),
            ],
            [
                'id'         => Str::uuid(),
                'name'       => 'Нет на складе',
                'color'       => 'F000CC',
                'position'   => 4,
                'updated_at' => now(),
                'created_at' => now(),
            ],
        ];

        DB::table('labels')->insert($labels);
    }
}

