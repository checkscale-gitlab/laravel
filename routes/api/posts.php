<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

use App\Http\Controllers\Api\PostController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth.jwt')->group(function () {

    Route::prefix('news')->group(function () {
        Route::get('/', [PostController::class, 'getAllNews']);
        Route::get('/{id}', [PostController::class, 'getNewsId']);
    });

    Route::prefix('blogs')->group(function () {
        Route::get('/', [PostController::class, 'getAllBlogs']);
        Route::get('/{id}', [PostController::class, 'getBlogId']);
    });

    Route::post('/create', [PostController::class, 'createPost']);
    Route::post('/update', [PostController::class, 'updatePost']);
    Route::get('/{id}', [PostController::class, 'getPostId']);
});
