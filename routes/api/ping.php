<?php

use App\Http\Controllers\Api\PingController;


Route::get('/', [PingController::class, 'show']);
Route::post('/', [PingController::class, 'store']);
Route::put('/', [PingController::class, 'update']);
Route::delete('/', [PingController::class, 'delete']);
