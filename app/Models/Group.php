<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models;

class Group extends BaseModel
{
    protected $fillable = [
        'id',
        'name',
        'discount',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
      //
    ];

}
