<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class BaseModel extends Model
{
    protected static function boot()
    {
        static::creating(function (Model $model): void {
            if (!$model->getKey()) {
                $uuid = (string)Str::uuid();
                $model->{$model->getKeyName()} = $uuid;
                $model->setAttribute($model->getKeyName(), $uuid);
            }
        });

        parent::boot();
    }

    public function getIncrementing(): bool
    {
        return false;
    }

    public function getKeyType(): string
    {
        return 'string';
    }
}
