<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models\NovaPoshta;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static findOrFail(string $id)
 */
class Warehouse extends Model
{
    use HasFactory, UuidTrait;

    protected $table = 'np_warehouses';

    protected $fillable = [
        'id',
        'description',
        'short_address',
        'phone',
        'ref',
        'city_ref',
        'city_description',
        'area',
        'area_description',
        'area_type',
        'region_description',
        'longitude',
        'latitude',
        'created_at',
        'updated_at',
    ];

    protected $visible = [
        'id',
        'description',
        'short_address',
        'phone',
        'ref',
        'city_ref',
        'city_description',
        'area',
        'area_description',
        'area_type',
        'region_description',
        'longitude',
        'latitude',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
        // ничего не скрываем, у нас все честно!
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

	public static function city(mixed $getAttribute)
	{
        return Warehouse::where('city_ref', $getAttribute)->get();
	}
}
