<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models;

class Categories extends BaseModel
{
    protected $fillable = [
        'id',
        'parent_id',
        'name',
        'meta_title',
        'meta_description',
        'description',
        'annotation',
        'url',
        'image',
        'visible',
        'position',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        //
    ];
}
