<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static findOrFail(string $id)
 */
class Post extends Model
{
    use HasFactory, UuidTrait;

    public const BLOG = 'blog';
    public const NEWS = 'news';

    protected $table = 'posts';

    protected $fillable = [
        'id',
        'name',
        'url',
        'meta_title',
        'meta_description',
        'title',
        'description',
        'annotation',
        'image',
        'type',
        'visible',
        'created_at',
        'updated_at',
    ];

    protected $visible = [
        'id',
        'name',
        'url',
        'meta_title',
        'meta_description',
        'title',
        'description',
        'annotation',
        'image',
        'type',
        'visible',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
        //
    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function scopeNews($query)
    {
        return $query->where('type', self::NEWS);
    }

    public function scopeBlogs($query)
    {
        return $query->where('type', self::BLOG);
    }
}
