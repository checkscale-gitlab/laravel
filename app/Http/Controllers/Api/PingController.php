<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class PingController extends Controller
{
    public function show(Request $request): Response
    {
        return $this->successResponse($request->all());
    }

    public function store(Request $request): Response
    {
        return response($request->all(), Response::HTTP_CREATED);
    }

    public function update(Request $request): Response
    {
        return $this->successResponse($request->all());
    }

    public function delete(): Response
    {
        return $this->successDeletedResponse();
    }
}
