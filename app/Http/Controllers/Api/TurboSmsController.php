<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TurboSms;

class TurboSmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTurboSmsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTurboSmsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TurboSms  $turboSms
     * @return \Illuminate\Http\Response
     */
    public function show(TurboSms $turboSms)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTurboSmsRequest  $request
     * @param  \App\Models\TurboSms  $turboSms
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTurboSmsRequest $request, TurboSms $turboSms)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TurboSms  $turboSms
     * @return \Illuminate\Http\Response
     */
    public function destroy(TurboSms $turboSms)
    {
        //
    }
}
