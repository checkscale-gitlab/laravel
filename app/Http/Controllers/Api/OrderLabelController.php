<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreOrderLabelRequest;
use App\Http\Requests\UpdateOrderLabelRequest;
use App\Models\OrderLabel;

class OrderLabelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreOrderLabelRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrderLabelRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderLabel  $orderLabel
     * @return \Illuminate\Http\Response
     */
    public function show(OrderLabel $orderLabel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateOrderLabelRequest  $request
     * @param  \App\Models\OrderLabel  $orderLabel
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOrderLabelRequest $request, OrderLabel $orderLabel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrderLabel  $orderLabel
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderLabel $orderLabel)
    {
        //
    }
}
