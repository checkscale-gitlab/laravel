<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCallbackRequest;
use App\Http\Requests\UpdateCallbackRequest;
use App\Models\Callback;

class CallbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCallbackRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCallbackRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Callback  $callback
     * @return \Illuminate\Http\Response
     */
    public function show(Callback $callback)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCallbackRequest  $request
     * @param  \App\Models\Callback  $callback
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCallbackRequest $request, Callback $callback)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Callback  $callback
     * @return \Illuminate\Http\Response
     */
    public function destroy(Callback $callback)
    {
        //
    }
}
