<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Exceptions\BadCredentialsException;
use App\Exceptions\EmailUnconfirmedException;
use App\Exceptions\IncorectSaveDataException;
use App\Exceptions\UserBlockedException;
use App\Http\Requests\Managers\CreateManagerRequest;
use App\Http\Requests\Managers\ManagerLoginRequest;
use App\Http\Resources\ManagerResource;
use App\Models\Managers;
use App\Services\ManagerService;
use App\Traits\MultiplePaginate;
use App\Services\Manager\AuthService;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;
use App\Http\Requests\Managers\GetAllManagerRequest;

class ManagersController extends Controller
{
    use MultiplePaginate;

    protected AuthService  $auth;
    private ManagerService $managerService;

    public function __construct(AuthService $authService, ManagerService $managerService)
    {
        $this->auth           = $authService;
        $this->managerService = $managerService;
    }

    /**
     * @throws UserBlockedException
     * @throws EmailUnconfirmedException
     * @throws BadCredentialsException
     */
    public function login(ManagerLoginRequest $request): Response
    {
        return $this->auth->loginManager($request);
    }

    public function logout(): Response
    {
        auth()->logout();
        return response(['status' => 'success']);
    }

    public function getManagerId($id): ManagerResource
    {
        return new ManagerResource(Managers::findOrFail($id));
    }

    public function getAllManagers(GetAllManagerRequest $request): ResourceCollection
    {
        return ManagerResource::collection(Managers::paginate($request->per_page));
    }


    /**
     * @throws IncorectSaveDataException
     */
    public function createManager(CreateManagerRequest $request): Response
    {
        return $this->managerService->createManager($request);
    }
}
