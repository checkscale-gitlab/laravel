<?php
/*
 * Copyright © 2022
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Requests\NovaPoshta;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed per_page
 */

class GetAllWarehousesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

	/**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page'     => 'required|integer',
            'per_page' => 'required|integer',
        ];
    }
}
