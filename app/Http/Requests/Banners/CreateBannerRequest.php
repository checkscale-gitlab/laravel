<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Requests\Banners;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateBannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page'        => ['required', 'string', 'min:3', 'max:50', Rule::unique('banners', 'page')],
            'name'        => 'required|string',
            'description' => 'required|string',
            'position'    => 'required|integer',
            'visible'     => 'required|boolean',
        ];
    }
}
