<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Resources\NovaPoshta;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @property mixed id
 * @property mixed description
 * @property mixed short_address
 * @property mixed phone
 * @property mixed ref
 * @property mixed city_ref
 * @property mixed city_description
 * @property mixed area
 * @property mixed area_description
 * @property mixed area_type
 * @property mixed region_description
 * @property mixed longitude
 * @property mixed latitude
 * @property mixed updated_at
 * @property mixed created_at
 */
class WarehouseResource extends JsonResource
{

    /**
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray($request): array
    {

        return [
            'id'                 => $this->id,
            "description"        => $this->description,
            "short_adress"       => $this->short_address,
            "phone"              => $this->phone,
            "ref"                => $this->ref,
            "city_ref"           => $this->city_ref,
            "city_description"   => $this->city_description,
            "area"               => $this->area,
            "area_description"   => $this->area_description,
            "area_type"          => $this->area_type,
            "region_description" => $this->region_description,
            "longitude"          => $this->longitude,
            "latitude"           => $this->latitude,
            'updated_at'         => Carbon::parse($this->updated_at)->format("Y-m-d H:i:s"),
            'created_at'         => Carbon::parse($this->created_at)->format("Y-m-d H:i:s"),
        ];
    }
}
