<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Resources\NovaPoshta;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @property mixed id
 * @property mixed description
 * @property mixed ref
 * @property mixed area
 * @property mixed area_description
 * @property mixed area_type
 * @property mixed updated_at
 * @property mixed created_at
 */
class CityResource extends JsonResource
{

    /**
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray($request): array
    {

        return [
            'id'               => $this->id,
            "description"      => $this->description,
            "ref"              => $this->ref,
            "area"             => $this->area,
            "area_description" => $this->area_description,
            "area_type"        => $this->area_type,
            'updated_at'       => Carbon::parse($this->updated_at)->format("Y-m-d H:i:s"),
            'created_at'       => Carbon::parse($this->created_at)->format("Y-m-d H:i:s"),
        ];
    }
}
