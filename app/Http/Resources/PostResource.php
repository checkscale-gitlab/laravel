<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * @property mixed id
 * @property mixed name
 * @property mixed url
 * @property mixed title
 * @property mixed meta_title
 * @property mixed meta_description
 * @property mixed description
 * @property mixed annotation
 * @property mixed image
 * @property mixed type
 * @property mixed visible
 * @property mixed updated_at
 * @property mixed created_at
 */
class PostResource extends JsonResource
{

    /**
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray($request) : array
    {

        return [
            'id'               => $this->id,
            'name'             => $this->name,
            'url'              => $this->url,
            'meta_title'       => $this->meta_title,
            "meta_description" => $this->meta_description,
            'title'            => $this->title,
            "description"      => $this->description,
            'annotation'       => $this->annotation,
            'image'            => $this->image,
            'type'             => $this->type,
            'visible'          => $this->visible,
            'updated_at'       => Carbon::parse($this->updated_at)->format("Y-m-d H:i:s"),
            'created_at'       => Carbon::parse($this->created_at)->format("Y-m-d H:i:s"),
        ];
    }
}
