<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Services\Validate;

use App\Exceptions\BadCredentialsException;
use App\Exceptions\EmailUnconfirmedException;
use App\Exceptions\UserBlockedException;
use App\Models\Managers;
use Illuminate\Support\Facades\Hash;


class AuthValidateService
{
    /**
     * @throws EmailUnconfirmedException
     * @throws UserBlockedException
     * @throws BadCredentialsException
     */
    public function validateManager(Managers $manager, $request): bool
    {
        if ($manager->blocked) throw new UserBlockedException();
        if (!$manager->email_verified_at) throw new EmailUnconfirmedException();
        if (!Hash::check($request->password, $manager->password)) throw new BadCredentialsException();
        if ($request->remember) auth()->setTTL(10080);
        return true;
    }
}
