<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Services;

use App\Exceptions\IncorectSaveDataException;
use App\Models\Managers;
use ArgumentCountError;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class ManagerService
{

    /**
     * @throws IncorectSaveDataException
     */

    public function createManager($data): Response
    {
        try {
            $manager = [
                'id'                => Str::uuid(),
                'email'             => $data->email,
                'password'          => Hash::make($data->password),
                'permission'        => implode(",", $data->permission),
                'name'              => $data->name,
                'description'       => $data->description,
                'blocked'           => false,
                'email_verified_at' => now()->format('Y-m-d H:i:s'),
                'remember_token'    => null,
                'created_at'        => now()->format('Y-m-d H:i:s'),
                'updated_at'        => now()->format('Y-m-d H:i:s'),
            ];

            Managers::query()->insert($manager);

            return response(['message' => 'manager created'], Response::HTTP_CREATED);

        } catch (Exception | ArgumentCountError $e) {
            Log::error('CREATE MANAGER ' . $e->getMessage());
            throw new IncorectSaveDataException();
        }
    }
}
