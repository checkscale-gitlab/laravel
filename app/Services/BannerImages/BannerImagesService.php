<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Services\BannerImages;

use App\Exceptions\IncorectSaveDataException;
use App\Http\Resources\BannerImageResource;
use App\Models\BannerImage;
use ArgumentCountError;
use Exception;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * @property BannerImageResource resource
 */
class BannerImagesService
{
    /**
     * @throws IncorectSaveDataException
     */
    public function createBannerImage(array $data): Response
    {

        try {
            $data["image"] = $this->saveBannerImage($data["image"], $data["title"], 'banner');
            BannerImage::query()->create($data);

            return response(['message' => 'banner created'], Response::HTTP_CREATED);

        } catch (Exception | ArgumentCountError $e) {
            Log::error('CREATE BANNER ' . $e->getMessage());
            throw new IncorectSaveDataException();
        }
    }

    /**
     * @throws IncorectSaveDataException
     */
    public function updateBannerImage(array $data): Response
    {
        try {
            $data["image"] = $this->saveBannerImage($data["image"], $data["title"], 'banner');
            BannerImage::query()->where('id', $data['id'])->update($data);
            return response(['message' => 'banner updated'], Response::HTTP_OK);
        } catch (Exception | ArgumentCountError $e) {
            Log::error('UPDATE BANNER ' . $e->getMessage());
            throw new IncorectSaveDataException();
        }
    }

    public function getAllImages(array $data): AnonymousResourceCollection
    {
        return Cache::remember('banner_images_' . $data["page"], config('cache.lifetime'), function () use ($data) {
            return BannerImageResource::collection(BannerImage::desc()->paginate($data["per_page"]));
        });
    }

    public function getImageId(string $id): BannerImageResource
    {
        return Cache::remember($id, config('cache.lifetime'), function () use ($id) {
            return new BannerImageResource(BannerImage::findOrFail($id));
        });
    }

    public function saveBannerImage($image, string $title, string $dir): string
    {
        $extension = $image->getClientOriginalExtension();
        $name      = Str::of($title)->slug('_') . '.' . $extension;

        return Storage::disk('public')->putFileAs($dir, $image, $name);
    }
}
