<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Services\Manager;

use App\Exceptions\BadCredentialsException;
use App\Exceptions\EmailUnconfirmedException;
use App\Exceptions\UserBlockedException;
use App\Models\Managers;
use App\Services\Validate\AuthValidateService;
use Illuminate\Http\Response;

class AuthService extends AuthValidateService
{
    /**
     * @throws UserBlockedException
     * @throws BadCredentialsException
     * @throws EmailUnconfirmedException
     */
    public function loginManager($request)
    {
        $manager = Managers::where('email', $request->email)->first();
        if (!$manager) throw new BadCredentialsException();
        $this->validateManager($manager, $request);
        $token = auth('manager')->tokenById($manager->id);
        return response(['token' => $token, 'manager' => $manager], Response::HTTP_OK);
    }
}
