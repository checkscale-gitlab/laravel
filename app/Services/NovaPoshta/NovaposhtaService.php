<?php
/*
 * Copyright © 2022
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Services\NovaPoshta;

use App\Http\Resources\NovaPoshta\CityResource;
use App\Http\Resources\NovaPoshta\WarehouseResource;
use App\Models\NovaPoshta\City;
use App\Models\NovaPoshta\Warehouse;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

/**
 * @property HttpClient client
 * @property mixed      apiKey
 */
class NovaposhtaService
{

    /**
     * NovaposhtaService constructor.
     */
    public function __construct(HttpClient $client)
    {
        $this->client = $client;
        $this->apiKey = config('novaposhta.apiKey');
    }

    public function getCities(): bool|string
    {
        $params = [
            "apiKey"       => $this->apiKey,
            "modelName"    => "Address",
            "calledMethod" => "getCities",
        ];

        return $this->client->send('POST', $params);
    }

    public function getWarehouses(): bool|string
    {
        $params = [
            "apiKey"       => $this->apiKey,
            "modelName"    => "Address",
            "calledMethod" => "getWarehouses",
        ];

        return $this->client->send('POST', $params);
    }

    public function getAllCity() : ResourceCollection
    {
        return Cache::remember('all_city', config('cache.lifetime'), function () {
            return CityResource::collection(City::all());
        });
    }

    public function getCityByName(string $name)
    {
        return Cache::remember($name, config('cache.lifetime'), function () use ($name) {
            return City::getName($name);
        });
    }

    public function getAllWarehouses(array $data) : ResourceCollection
    {
        return Cache::remember('warehouses_' . $data["page"], config('cache.lifetime'), function () use ($data) {
            return WarehouseResource::collection(Warehouse::paginate($data["per_page"]));
        });
    }

    public function saveCities($cities)
    {
        foreach ($cities['data'] as $city) {

            City::query()->updateOrInsert(
                [
                    'ref'         => $city["Ref"],
                    "area"        => $city["Area"],
                    "description" => $city["Description"],
                ],
                [
                    "id"               => Str::uuid(),
                    "description"      => $city["Description"],
                    "ref"              => $city["Ref"],
                    "area"             => $city["Area"],
                    "area_description" => $city["AreaDescription"],
                    "area_type"        => $city["SettlementTypeDescription"],
                    "created_at"       => Carbon::now(),
                    "updated_at"       => Carbon::now(),

                ]
            );


        }

    }

    public function saveWarehouses($warehouses)
    {
        foreach ($warehouses['data'] as $warehouse) {

            Warehouse::query()->updateOrInsert(
                [
                    'ref'         => $warehouse["Ref"],
                    "city_ref"    => $warehouse["CityRef"],
                    "description" => $warehouse["Description"],
                ],
                [
                    "id"                 => Str::uuid(),
                    "description"        => $warehouse["Description"],
                    "short_address"      => $warehouse["ShortAddress"],
                    "phone"              => $warehouse["Phone"],
                    "ref"                => $warehouse["Ref"],
                    "city_ref"           => $warehouse["CityRef"],
                    "city_description"   => $warehouse["CityDescription"],
                    "area"               => $warehouse["SettlementDescription"],
                    "area_description"   => $warehouse["SettlementAreaDescription"],
                    "area_type"          => $warehouse["SettlementTypeDescription"],
                    "region_description" => $warehouse["SettlementRegionsDescription"],
                    "longitude"          => $warehouse["Longitude"],
                    "latitude"           => $warehouse["Latitude"],
                    "created_at"         => Carbon::now(),
                    "updated_at"         => Carbon::now(),

                ]
            );


        }

    }
}
