<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Services\Post;

use Exception;
use App\Models\Post;
use ArgumentCountError;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use App\Http\Resources\PostResource;
use Illuminate\Support\Facades\Cache;
use App\Exceptions\IncorectSaveDataException;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PostService
{
    /**
     * @throws IncorectSaveDataException
     */
    public function createPost($data) : Response
    {

        try {
            Post::query()->create($data->toArray());
            return response(['message' => 'post created'], Response::HTTP_CREATED);
        } catch (Exception | ArgumentCountError $e) {
            Log::error('CREATE POST ' . $e->getMessage());
            throw new IncorectSaveDataException();
        }
    }

    /**
     * @throws IncorectSaveDataException
     */
    public function updatePost($data) : Response
    {
        try {
            Post::query()->where('id', $data->id)->update($data->toArray());
            return response(['message' => 'post updated'], Response::HTTP_OK);
        } catch (Exception | ArgumentCountError $e) {
            Log::error('UPDATE POST ' . $e->getMessage());
            throw new IncorectSaveDataException();
        }
    }

    public function getNewsId(string $id) : PostResource
    {
        return Cache::remember('news_' . $id, config('cache.lifetime'), function () use ($id) {
            return new PostResource(Post::news()->findOrFail($id));
        });
    }

    public function getBlogId(string $id) : PostResource
    {
        return Cache::remember('blogs_' . $id, config('cache.lifetime'), function () use ($id) {
            return new PostResource(Post::blogs()->findOrFail($id));
        });
    }

    public function getAllBlogs(array $data) : ResourceCollection
    {
        return Cache::remember('blogs_' . $data["page"], config('cache.lifetime'), function () use ($data) {
            return PostResource::collection(Post::blogs()->paginate($data["per_page"]));
        });
    }

    public function getAllNews(array $data) : ResourceCollection
    {
        return Cache::remember('news_' . $data["page"], config('cache.lifetime'), function () use ($data) {
            return PostResource::collection(Post::news()->paginate($data["per_page"]));
        });
    }

}
