<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Exceptions;

use Exception;

class IncorectSaveDataException extends Exception
{
    protected $message = 'incorrect_data';
}
