<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Exceptions;

use Exception;

class NotFoundException extends Exception
{
    protected $message = 'not_found';
}
