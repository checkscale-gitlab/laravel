<?php

namespace App\Console\Commands\NovaPoshta;

use App\Services\NovaPoshta\NovaposhtaService;
use Illuminate\Console\Command;

class GetCities extends Command
{

    protected $signature = 'novaposhta:getCities';

    protected $description = 'Получение всех городов с Новой Почты';

    public function handle(NovaposhtaService $novaposhtaService)
    {
        $cities = json_decode($novaposhtaService->getCities(), true);
        if ($cities['success']) $novaposhtaService->saveCities($cities);
    }

}
