<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule): void
    {
        // Получение базы городов с Новой почты
        $schedule->command('novaposhta:getCities')->dailyAt('3:00');

        // Получение базы отделений с Новой почты
        $schedule->command('novaposhta:getWarehouses')->dailyAt('3:20');

        // очистка телескопа
        if (config('env') !== 'production') {
            $schedule->command('telescope:prune --hours=48')->daily();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands(): void
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
