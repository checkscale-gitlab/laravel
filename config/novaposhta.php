<?php
/*
 * Copyright © 2022
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

return [
    'path' => env('NOVAPOSHTA_PATH', 'https://api.novaposhta.ua/v2.0/json/'),
    'apiKey' => env('NOVAPOSHTA_KEY', ''),
];
