<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

return [
    'email'    => env('APP_EMAIL'),
    'password' => env('APP_PASSWORD'),

];
