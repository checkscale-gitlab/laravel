<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

return [
    'host' => env('APP_URL', 'http://localhost:8080'),
];
